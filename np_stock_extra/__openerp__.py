{
    'name' : 'stock updates for NP company',
    'version' : '1.0.0',
    'author' : 'Ivan Yelizariev',
    'category' : 'Base',
    'website' : 'https://it-projects.info',
    'description': """
    """,
    'depends' : ['stock'],
    'data':[
        'views.xml'
        ],
    'demo':[
    ],
    'installable': True
}
